//
//  MonsterViewModel.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

class MonsterViewModel {
    
    public var id: Int {
        return self.monster.id
    }
    
    public var name: String {
        return self.monster.name;
    }
    public var health: Int {
        return self.monster.health;
    }
    
    public var imagePath: URL? {
        return self.monster.imagePath;
    }
    
    private var monster: Monster
    
    init (monster: Monster) {
        self.monster = monster
    }
    
    // TODO: Binding
}
