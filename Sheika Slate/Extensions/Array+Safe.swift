//
//  Array+Safe.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

extension Array {
    
    public func get(_ index: Int) -> Element? {
        return index < self.count ? self[index] : nil
    }
}
