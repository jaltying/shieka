//
//  RepositoryOnlineEndpoint.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

class RepositoryOnlineEndpoint {
    
    // TODO: Implement API Versioning or user specific arguments upon initialization
    
    public func getMonstersEndpoint() -> URL {
        
        guard let endpoint = URL(string: "http://www.mocky.io/v2/5a4382f82e0000ad0f765f71") else {
            fatalError("Generated an invalid endpoint!")
        }
        
        return endpoint
    }
}
