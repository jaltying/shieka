//
//  MonsterRepository.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

typealias MonstersFetchCompletion = ([Monster]?) -> ()

class MonsterRepository {
    
    public static let sharedInstance = MonsterRepository()
    
    private init(){}
    
    public func getMonsters(completion: @escaping MonstersFetchCompletion) {
        
        let onlineRepository = MonsterOnlineRepository()
        onlineRepository.getMonsters { (jsonArray) in
            
            if let jsonArray = jsonArray {
                self.parseMonsterJSONArray(jsonArray, completion: completion)
            }
        }
        
        // TODO: Add Offline Repository
    }
    
    private func parseMonsterJSONArray(_ jsonArray: JSONArray, completion: @escaping MonstersFetchCompletion) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            var parsedMonsters: [Monster]?
            
            for json in jsonArray {
                
                guard let monster = Monster(json: json) else {
                    continue
                }
                
                if (parsedMonsters == nil) {
                    parsedMonsters = [Monster]()
                }
                
                parsedMonsters?.append(monster)
            }
            
            DispatchQueue.main.async {
                completion(parsedMonsters)
            }
        }
    }
}
