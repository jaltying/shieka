//
//  MonsterAlamofireRepository.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Alamofire

typealias JSONArray = [[String : Any]]
typealias MonstersJSONArrayFetchCompletion = (JSONArray?) -> ()

class MonsterAlamofireRepository {
    
    private var endpointRepository = RepositoryOnlineEndpoint()
    
    public func getMonsters(completion: @escaping MonstersJSONArrayFetchCompletion) {
     
        let endpoint = self.endpointRepository.getMonstersEndpoint();
        
        Alamofire.request(endpoint).validate().responseJSON { (response) in
            
            let result = response.result
            
            switch (result) {
                
            case .success(let value):
                
                let castedJSONArray = value as? JSONArray
                completion(castedJSONArray)
                
            case .failure:
                completion(nil)
            }
            
        }
    }
}
