//
//  MonsterOnlineRepository.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

class MonsterOnlineRepository {
    
    public func getMonsters(completion: @escaping MonstersJSONArrayFetchCompletion) {
        
        let monsterAlamofireOnlineRepository = MonsterAlamofireRepository()
        monsterAlamofireOnlineRepository.getMonsters(completion: completion)
    }
}
