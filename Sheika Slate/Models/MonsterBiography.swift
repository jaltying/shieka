//
//  MonsterBiography.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

class MonsterBiography {
    
    public var id: Int
    public var imagePath: URL
    
    // TODO: Add more info
    
    init (id: Int, imagePath: URL) {
        
        self.id = id
        self.imagePath = imagePath
    }
}
