//
//  Monster.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import Foundation

class Monster {
    
    private static let keyId = "id"
    private static let keyName = "name"
    private static let keyHealth = "health"
    private static let keyImagePath = "imagePath"
    
    public var id: Int
    public var name: String
    public var health: Int
    public var imagePath: URL
    
    init (id: Int, name: String, health: Int, imagePath: URL) {
        
        self.id = id
        self.name = name
        self.health = health
        self.imagePath = imagePath
    }
    
    init? (json: [String : Any]) {
        
        guard let id = json[Monster.keyId] as? Int,
            let name = json[Monster.keyName] as? String,
            let health = json[Monster.keyHealth] as? Int,
            let readableImagePath = json[Monster.keyImagePath] as? String,
            let imagePath = URL(string: readableImagePath) else {
                return nil
        }
        
        self.id = id
        self.name = name
        self.health = health
        self.imagePath = imagePath
    }
}
