//
//  MonsterTableViewCell.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import UIKit
import SDWebImage

class MonsterTableViewCell: UITableViewCell {
    
    public static let identifier = "MonsterTableViewCell"
    public static let nibName = "MonsterTableViewCell"
    
    @IBOutlet private var labelName: UILabel!;
    @IBOutlet private var labelHealth: UILabel!;
    @IBOutlet private var imageViewMonsterImage: UIImageView!;
    
    public var monsterViewModel: MonsterViewModel? {
        didSet {
            self.updateViews()
        }
    }
    
    // MARK: - Lifecycle
    
    // TODO: - Binding
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reset()
    }
    
    // MARK: - UI Methods
    
    private func updateViews() {
        
        guard let name = self.monsterViewModel?.name,
            let health = self.monsterViewModel?.health,
            let imagePath = self.monsterViewModel?.imagePath else {
                return;
        }
        
        self.labelName.text = name
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        self.labelHealth.text = "Health: \(health)"
        self.imageViewMonsterImage?.sd_setImage(with: imagePath)
        
        self.accessoryType = .disclosureIndicator
    }
    
    private func reset() {
        
        self.labelName.text = nil
        self.labelHealth.text = nil
        self.imageViewMonsterImage.image = nil
    }
}
