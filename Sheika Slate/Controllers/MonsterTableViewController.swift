//
//  MonsterTableViewController.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class MonsterTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    private var monsters: [Monster]?
    private var monsterRepository = MonsterRepository.sharedInstance
    
    private var monsterRefreshControl: UIRefreshControl?
    
    public class func newInstance() -> MonsterTableViewController {
        
        let monsterTableViewController = MonsterTableViewController(style: .plain)
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        let title = "BOTW Monster List"
        monsterTableViewController.title = title
        
        return monsterTableViewController
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initializeTableViewProperties()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchMonsters(forced: false)
    }
    
    // MARK: - UITableViewDataSource Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.monsters?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: MonsterTableViewCell.identifier, for: indexPath) as! MonsterTableViewCell
        
        // Capture the monster
        
        let currentRow = indexPath.row
        guard let monster = self.monsters?.get(currentRow) else {
            fatalError("Captured a nil `monster` object!")
        }
        
        // Configure the cell
        
        let monsterViewModel = MonsterViewModel(monster: monster)
        dequeuedCell.monsterViewModel = monsterViewModel
        
        return dequeuedCell
    }
    
    // MARK: - UITableViewDelegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Capture the monster
        
        let currentRow = indexPath.row
        guard let monster = self.monsters?.get(currentRow) else {
            fatalError("Captured a nil `monster` object!")
        }
        
        // TODO: Present in-depth info about the tapped monster
        
        let monsterBiographyViewController = MonsterBiographyViewController.newInstance(monster: monster)
        self.navigationController?.pushViewController(monsterBiographyViewController, animated: true)
    }
    
    // MARK: - DZNEmptyDataSetSource Methods
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let font = UIFont.preferredFont(forTextStyle: .title1)
        let foregroundColor = UIColor.lightGray
        
        let attributes = [NSAttributedStringKey.font : font,
                          NSAttributedStringKey.foregroundColor : foregroundColor]
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        let title = ":("
        return NSAttributedString(string: title, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let font = UIFont.preferredFont(forTextStyle: .body)
        let foregroundColor = UIColor.lightGray
        
        let attributes = [NSAttributedStringKey.font : font,
                          NSAttributedStringKey.foregroundColor : foregroundColor]
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        let description = "No monsters fetched"
        return NSAttributedString(string: description, attributes: attributes)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let font = UIFont.preferredFont(forTextStyle: .caption1)
        
        var foregroundColor = UIColor.lightGray
        if (state == .highlighted) {
            foregroundColor = UIColor.darkGray
        }
        
        let attributes = [NSAttributedStringKey.font : font,
                          NSAttributedStringKey.foregroundColor : foregroundColor] as [NSAttributedStringKey : Any]
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        let title = "Tap to update"
        return NSAttributedString(string: title, attributes: attributes)
    }
    
    
    // MARK: - DZNEmptyDataSetDelegate Methods
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.fetchMonsters(forced: false)
        
        // There's no device available todey on my side, so I can't test this thoroughly.
        // The current build of simulator was very slow on machines with dedicated GPU. IDK Why
    }
    
    // MARK: - Action Methods
    
    @objc private func refresh(sender: UIRefreshControl) {
        self.fetchMonsters(forced: true)
    }
    
    // MARK: -  Data Methods
    
    private func fetchMonsters(forced: Bool) {
     
        if (forced) {
            
            self.monsterRefreshControl?.beginRefreshing()
            
            self.tableView.reloadEmptyDataSet()
        }
        
        self.monsterRepository.getMonsters { (monsters) in
            
            // TODO: Indicate Error?
            
            self.monsters = monsters
            
            if (forced) {
                self.monsterRefreshControl?.endRefreshing()
            }
            
            self.tableView.reloadEmptyDataSet()
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UI Methods
    
    /// Initialize the tableView extra properties
    private func initializeTableViewProperties() {
        
        let nib = UINib(nibName: MonsterTableViewCell.nibName, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: MonsterTableViewCell.identifier)
        
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.separatorStyle = .none
        
        self.tableView.tableFooterView = UIView()
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        self.initializeMonsterRefreshControl()
    }
    
    private func initializeMonsterRefreshControl() {
        
        self.monsterRefreshControl = UIRefreshControl()
        
        let action = #selector(refresh(sender:))
        self.monsterRefreshControl?.addTarget(self, action: action, for: .valueChanged)
        
        self.refreshControl = self.monsterRefreshControl
    }
}

