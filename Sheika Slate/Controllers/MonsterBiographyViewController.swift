//
//  MonsterBiographyViewController.swift
//  Sheika Slate
//
//  Created by Jaja on 27/12/2017.
//  Copyright © 2017 jlytng. All rights reserved.
//

import UIKit
import SDWebImage

class MonsterBiographyViewController: UIViewController {
    
    @IBOutlet private var labelMonsterName: UILabel!;
    @IBOutlet private var imageViewMonsterImage: UIImageView!;
    
    public var monster: Monster?
    
    public class func newInstance(monster: Monster) -> MonsterBiographyViewController {
        
        let monsterBiographyViewController = MonsterBiographyViewController()
        monsterBiographyViewController.monster = monster
        
        // TODO: Transfer the Hard-coded string in a repository for localization
        let title = "About"
        monsterBiographyViewController.title = title
        
        return monsterBiographyViewController
    }
    
    // MARK: - View Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fetchMonsterBiography()
    }
    
    // MARK: - UI Methods
    
    private func fetchMonsterBiography() {
        
        // TODO: Request the proper biography based on monster passed
        
        // For the mean time present the monster's image and name
        
        self.imageViewMonsterImage.sd_setImage(with: self.monster?.imagePath)
        
        self.labelMonsterName.text = self.monster?.name
    }
}
